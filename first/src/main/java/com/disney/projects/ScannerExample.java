package com.disney.projects;

import java.util.Scanner;

public class ScannerExample {

	public static void main(String[] args) {

	Scanner scanner = new Scanner(System.in);
	System.out.println("Whats is your name?");
	
	String name = scanner.next(); 
	
		System.out.println("How old are you?");
		int years = scanner.nextInt();
		
		System.out.println("Hi " + name);
		
		System.err.println("You have " + years + " years old");
	}

}
