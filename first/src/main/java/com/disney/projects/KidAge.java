package com.disney.projects;

import java.util.Scanner;

public class KidAge {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter the age of the first child");
		int ageFirstChild = scanner.nextInt();
		
		
		System.out.println("Enter the age of the second child");
		int ageSecondChild = scanner.nextInt();	
		
		if (ageFirstChild > ageSecondChild) {
			System.out.println("the first child is older than the second");
			
		}else if (ageFirstChild == ageSecondChild){
		
			System.out.println("they have the same age");
			
		}else{
			System.out.println("the second child is older than the first");
		}

	}

}
