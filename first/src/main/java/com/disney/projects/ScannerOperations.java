package com.disney.projects;

import java.util.Scanner;

public class ScannerOperations {

	public static void product(int a, int b) {
       System.out.println("producto = " + (a*b));
	}
	
	public static void divider(int a, int b) {
	   System.out.println("divider = " + (a/b));
    }
	
	public static void pow(int a, int b) {
	   System.out.println("pow = " + Math.pow(a,b));
    }
	
	public static void squirt(int a, int b) {
	   System.out.println("root A = " + Math.sqrt(a));
	   System.out.println("root B = " + Math.sqrt(b));
    }
	
	
	public static void main(String[] args) {
	
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Give A Please");
		int a = scanner.nextInt();
		System.out.println("Give B Please");
		int b = scanner.nextInt();
		
		product(a, b);
		divider(a, b);
		pow(a, b);
		squirt(a, b);
}
}
