package com.disney.projects;

import java.util.Scanner;

public class ExerciseSwitch2 {

	public static void main(String[] args) {
	       Scanner keyboard=new Scanner (System.in);
	       int week;
	       System.out.println ("Enter the number of the week");
	       week = keyboard.nextInt();
	       System.out.println(week);
	     
	       switch (week)
	     
	       {
	           case 1: System.out.println("Monday");
	                    break;
	         
	           case 2: System.out.println("Tuesday");
	                    break;
	         
	           case 3: System.out.println("Wednesday");
	                    break;
	         
	           case 4: System.out.println("Thursday");
	                    break;
	         
	           case 5: System.out.println("Friday");
	                    break;
	         
	           case 6: System.out.println("Saturday");
	                    break;
	         
	           case 7: System.out.println("Sunday");
	                    break;
	         
	            }
	     
	     }
	   
	}

