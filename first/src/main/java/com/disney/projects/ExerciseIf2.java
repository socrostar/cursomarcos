package com.disney.projects;

import java.util.Scanner;

public class ExerciseIf2 {

	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        int N;
	        System.out.print("Whole number: ");
	        N = scanner.nextInt();
	        if(N%10==0)
	           System.out.println("Multiple of 10");   
	        else
	         System.out.println("It is not a multiple of 10");
	    }
	}